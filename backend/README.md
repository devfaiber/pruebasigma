## backend nodejs
almacena un contacto por formulario que lo proporciona react en el frontend, nodejs se encarga de almacenarlo en la base de datos

### configuracion
para la configuracion se debe seguir los siguientes puntos
- debe ejecutarse dentro del directorio `npm install`
- para correr dicho sistema se requiere la configuracion de la base de datos en el archivo `/config/database`
- correr el backend con el comando `nodemon src/index.js` o con `npm start`

### construccion del sistema
el backend esta construido bajo el modelo mvc y usando librerias 
- morgan: manejo de log http en consola
- express: servidor web
- mysql: modulo para conectar driver de mysql
- nodemon: usado para estar a la escucha a los cambios

### configuracion de base de datos
esto ya se encuentra configurado con la base de datos de prueba, pero si requieres observar la conexion, se puede cambiar sus datos en `.env` que es el archivo de configuracion
