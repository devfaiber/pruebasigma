const express = require("express");
const morgan = require('morgan');
const app = express();

app.use(express.urlencoded({extended: false}));
app.use(express.json())

//peticiones HTTP
app.use(morgan('combined'));

// CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});


// rutas
app.get("/", (req,res)=>{
    res.send("hola amigos");
});

app.use("/contacts", require("./routes/contactRouter"));
app.use("/states", require("./routes/stateRouter"));



module.exports = app;