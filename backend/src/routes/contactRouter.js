const router = require("express").Router();

const controller = require("./../controllers/contactController");

router.post("/save", controller.save);


module.exports = router;