const router = require("express").Router();

const controller = require("./../controllers/stateController");

router.get("/all", controller.all);

module.exports = router;