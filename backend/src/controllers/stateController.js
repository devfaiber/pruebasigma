const fs = require('fs');
const path = require("path");
const config = require("../../config/config");

const controller = {
    all: (req, res)=>{
        const response = {
            code: 200,
            message: null,
            data: {}
        }
        const url = path.join(__dirname,"../assets/files");

        fs.readFile(`${url}/colombia.json`, 'utf-8', (err, data) => {
            if(err) {
                response.code = 404;
                response.message = config.SMS_ERROR_FILE;
            } else {
              console.log(data);
              response.code = 200;
              response.data = JSON.parse(data);
            }

            return res.status(response.code).json(response);
        });

    }
}

module.exports = controller;