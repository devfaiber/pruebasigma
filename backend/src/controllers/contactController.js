const db = require("../../config/Connection");
const config = require("../../config/config");
const validator = require("validator");
ERROR_DB = "algo salio mal con la base de datos";

const controller = {
    save: (req, res)=>{
        
        const body = req.body;

        console.log(req.body);

        const response = {
            code: 0,
            message: ""
        };

        const dataQuery = [
            body.name || "",
            body.email || "",
            body.state || "",
            body.city || ""
        ];
       

        for (let i = 0; i < dataQuery.length; i++) {
            if(validator.isEmpty(dataQuery[i])){
                response.code = config.CODE_ERROR;
                response.message = config.FIELDS_EMPTY;
                return res.status(200).json(response);
            }
        }



        db.query("INSERT INTO contacts(name, email, state, city) VALUES(?,?,?,?)", dataQuery, (err, results, fields)=>{
            

            if(err){
                response.code = CODE_ERROR;
                response.message = config.SMS_ERROR_DB;
                return res.status(200).json(response); 
            }

            response.code = config.COD_SUCCESSFUL;
            response.message = config.SMS_SUCCESSFUL_SAVE;

            return res.status(200).json(response);

        });
    }
};


module.exports = controller;