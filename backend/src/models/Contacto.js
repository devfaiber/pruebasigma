class Contacto{

    constructor(name, email, state, city){
        this.id = null;
        this.name = name;
        this.email = email;
        this.state = state;
        this.city = city;
    }

    getId(){
        return this.id;
    }

    getName(){
        return this.name;
    }

    getEmail(){
        return this.email;
    }
    getState(){
        return this.state;
    }
    getCity(){
        return this.city;
    }

    setName(name){
        this.name = name;
    }
    setEmail(email){
        this.email = email;
    }
    setState(state){
        this.state = state;
    }
    setCity(city){
        this.city = city;
    }

}