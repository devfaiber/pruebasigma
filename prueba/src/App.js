import React from 'react';
import Header from './components/Header';
import SectionForm from './components/SectionForm';
import SectionDescription from './components/SectionDescription';

function App() {
  return (
    <React.Fragment>
      <Header/>
      <main>
        <SectionDescription/>
        <SectionForm/>
      </main>
    </React.Fragment>
  );
}

export default App;
