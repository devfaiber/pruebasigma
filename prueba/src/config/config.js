const config = {
    API_BASE_URL: "http://127.0.0.11:3001",
    CODE_SUCCESSFUL: 200,
    CODE_ERROR: 404
}

export default config;