import React from 'react';
class SectionDescription extends React.Component {
    render(){
        return (
            <section className="section-description row">
                <div className="col-12">
                    <h1 className="section-title">Prueba de desarrollo Sigma</h1>
                    <article>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium incidunt, pariatur omnis
                            qui aliquam enim tempora nemo cum voluptatibus nulla possimus exercitationem molestiae,
                            consequuntur distinctio necessitatibus id asperiores veniam numquam.
                        </p>
                    </article>
                </div>
            </section>
        );
    }
}

export default SectionDescription;