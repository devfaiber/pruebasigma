import React from 'react';
import config from '../config/config';
import Contact from '../models/Contact';

class SectionForm extends React.Component {
    deparmentAll = {};
    contactRef = new Contact();
    state = {

        deparments: [],
        cities: [],
        messageSaveSuccess: "",
        codeResponseSave: 0
    };


    constructor(props){
        super(props);
        this.changeState = this.changeState.bind(this);
        this.save = this.save.bind(this);
    }

    componentDidMount(){
        this.getState();
        
    }

    getState(){
        fetch(`${config.API_BASE_URL}/states/all`)
            .then(resp=>resp.json() )
            .then(data=>{
                
                this.deparmentAll = data.data;
                
                const deparments = Object.keys(this.deparmentAll);
                this.setState({
                    deparments: deparments
                })

            }).catch(err=>{
                console.log(err);
            })
    }

    options(item, index){
        return (
            <option key={index} value={item}>{item}</option>
        )
    }
    // evento change a select state
    changeState(event){
        
        let value = event.target.value;
        let indice = this.state.deparments.indexOf(value);
        const INDEX_LIST_CITIES = 1;

        let arrayDeparment = Object.entries(this.deparmentAll);
        let cities = arrayDeparment[indice][INDEX_LIST_CITIES];

        this.setState({
            cities: cities
        })
        console.log(cities);
    }

    save(event){
        event.preventDefault();
        
        let dataSend = {
            name : this.contactRef.name.current.value,
            email : this.contactRef.email.current.value,
            state: this.contactRef.state.current.value,
            city: this.contactRef.city.current.value
        }

        console.log(dataSend);
        fetch(`${config.API_BASE_URL}/contacts/save`,{
            method: "post",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(dataSend)
        }).then(resp=>resp.json())
        .then(data=>{
            console.log(data);

            this.setState({
                codeResponseSave: data.code,
                messageSaveSuccess: data.message
            });

            if(data.code == config.CODE_SUCCESSFUL){
                this.clearFields();
            }


        }).catch((error)=>{
            console.log(error);
        })
        

        
    }
    doAlert(){
        let code = this.state.codeResponseSave;
        let message = this.state.messageSaveSuccess;
        if(code && message){
            return (
                <div className={code==config.CODE_ERROR ? 'alert alert-danger' : 'alert alert-success' }>
                    {message}
                </div>    
            )
        }
    }
    clearFields(){
        this.contactRef.name.current.value = "";
        this.contactRef.email.current.value = "";
        this.contactRef.state.current.value = "";
        this.contactRef.city.current.value = "";
    }

    render(){
        
        return (
            <section className="section-form row">
                <div className="img-form col-md-6 col-12">
                    <img src="assets/images/sigma-image.png" alt="formulario-img" className="img-fluid"/>
                </div>
                <div className="form-contacto col-md-6 col-12 mt-sm-3">
                    
                    

                    <form method="post" onSubmit={this.save}>

                        {this.doAlert()}

                        <div className="form-group">
                            
                            <label htmlFor="deparment">Departamento*</label>
                            
                            {/* <input type="text" name="deparment" id="deparment" placeholder="Antioquia" className="form-control"/>--!> */}
                            <select onChange={this.changeState} name="state" id="state" className="form-control" ref={this.contactRef.state}>
                                <option value=""> -- -- --</option>
                                {this.state.deparments.map((item, index)=>{
                                        return this.options(item, index);
                                    })
                                }
                            </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="city">Ciudad*</label>
                            {/* <input type="text" name="city" id="city" placeholder="ciudad" className="form-control"/> */}
                            <select name="city" id="city" className="form-control" ref={this.contactRef.city} required>
                                <option value=""> -- -- --</option>
                                {this.state.cities.map((item, index)=>{
                                        return this.options(item, index);
                                    })
                                }
                            </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="name">Nombre*</label>
                            <input type="text" name="name" id="name" placeholder="Pepito de Jesus" className="form-control" ref={this.contactRef.name} required />
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">Correo*</label>
                            <input type="email" name="email" id="email" placeholder="pepitodejesus@gmail.com" className="form-control" ref={this.contactRef.email} required />
                        </div>
                        <div className="form-group">
                            <button type="submit" name="enviar" id="btn-enviar" className="btn btn-pink btn-md">Enviar</button>
                        </div>
                    </form>
                </div>
            </section>
        );
    }
}

export default SectionForm;