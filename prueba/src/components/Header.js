import React from 'react';
class Header extends React.Component {
    render(){
        return (
            <header className="row">
                <div className="col-12 logo">
                    <img src="assets/images/sigma-logo.png" alt="sigma"/>
                </div>
            </header>
        );
    }
}

export default Header;