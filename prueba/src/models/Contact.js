import React from 'react';

class Contact{
    constructor(){
        this.name = React.createRef();
        this.email = React.createRef();
        this.state = React.createRef();
        this.city = React.createRef();
    }
}

export default Contact;